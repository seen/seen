# “Non Resource” scenarios

In RESTFul theory, any request must be seen and manipulated as a resource. In real life, it’s not always possible, especially when we have to deal with actions such as translations, computations, conversions, complex business services or strongly integrated services.

In these cases, your operation must be represented by a verb rather than a name. For instance :

	POST /calculator/sum
	[1,2,3,5,8,13,21]
	&lt; 200 OK
	&lt; {"result" : "53"}
	Or else :

	POST /convert?from=EUR&amp;to=USD&amp;amount=42
	&lt; 200 OK
	&lt; {"result" : "54"}

We therefore come to use actions instead of resources. In this context, we will use the HTTP POST method.

	CURL –X POST \
	-H "Content-Type: application/json" \
	https://api.fakecompany.com/v1/users/42/carts/7/commit
	&lt; 200 OK
	&lt; { "id_cart": "7",<i> [...] <i> }</i></i>
 

To design properly this exception in your API, the simplest solution is to consider that any POST request is an action with an implicit or explicit verb.

For a collection of entity resources for instance, the default action is a creation:

	POST /users/create                                        POST /users
	&lt; 201 OK                                      ==          &lt; 201 OK
	&lt; { "id_user": 42 }                                       &lt; { "id_user": 42 }

Or for an email resource, the default action will be to send it to its recipient.

	POST /emails/42/send                             POST /emails/42I
	&lt; 200 OK                                ==          &lt; 200 OK
	&lt; { "id_email": 42, "state": "sent" }         &lt; { "id_email": 42, "state": "sent" }
 

However, it is important to bear in mind that explicitly specifying a verb in your API design must remain an exception. In most cases, it can and must be avoided. If several resources expose one action or more, your API design is flawed: you took an RPC approach rather than a REST approach, and need to quickly take action by going over your API design.

In order to avoid any confusion in developers’ minds between resources (which you can access the CRUD way) and actions, it is highly recommended to clearly separate these two concepts in the developer documentation.

