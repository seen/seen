# کاربر

هر فردی که در سیستم عضو باشد یک پروفایل مختص به خود دارد. در این پروفایل می‌تواند اطلاعات شخصی خود را ویرایش کند. این اطلاعات به دو دسته تقسیم می‌شود.

- پروفایل
- حساب کاربری

پروفایل بسته به هر نرم‌افزار می‌تواند متفاوت باشد در حالی که حساب کاربری به صورت یکتا برای تمام نرم‌افزارها تعریف می‌شود.


## جستجوی کاربران

جستجوی کاربران نیز با استفاده از تکنیک‌های صفحه بندی انجام می‌شود. از این رو دسته‌ای از خصوصیت‌ها برای جستجو، فیلتر و مرتب سازی در نظر گرفته شده است. این خصوصیت‌ها برای فیلتر کردن عبارتند از:

- administrator
- staff
- active

خصوصیت‌هایی که در جستجو به کار گرفته می‌شوند:

- login
- first_name
- last_name
- email

خصوصیت‌هایی که در مرتب سازی نتایج به کار گرفته می‌شوند عبارتند از:

- id
- login
- first_name
- last_name
- date_joined
- last_login
